public class Main {
//N players and a pistol that shoots every N time. 
	//Begging from one of them they are forced to aim to their head, try to shoot and then transmit the pistol to another player. 
	//So every N time one of them dies. This is continuing till the last man alive.
	//This algorithm shows which of them has to begin with the pistol in order that one specific player survives.
	public static void main(String[] args) {
		int target = 14; //insert the number of total players here, indexing begins from 1 in this case
		int zivy = 6; //insert the order number of player that you want to live, indexing begins from 1 in this case
		int alive = 2;
		int naboje = 5; //insert the number of the bullet count, indexing begins from 1 in this case
		int cur = 0;
		
		while (alive <= target) {
			for (int i = 0; i < naboje-1; i++) {
				cur--;
				if (cur == -1) {
					cur = alive-1;
				}
			}
			
			alive++;
		}
		
		cur+=zivy+1;
		if (cur>target) {
			cur-=target;
		}
		
		System.out.println(cur);//prints out the order number of player that has to begin with the gun, in order to our selected player to survive

	
		
	}

}
